#ifndef HACKSAW_UTIL_H
#define HACKSAW_UTIL_H

#include <stdio.h>

void prv_mkdir(const char* dir);

void file_copy(char* src, char* dest);

#endif
